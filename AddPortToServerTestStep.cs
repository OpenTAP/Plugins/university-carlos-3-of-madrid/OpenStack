//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;


namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack",  "Compute" }, Name: "Add Port")]
    [AllowAsChildIn(typeof(CreateServerTestStep))]
    [Browsable(false)]
    public class AddPortToServerTestStep : TestStep
    {

        private Network _Network;
        private List<Network> _listOfNetworks;
        private Subnet _Subnet;
        private List<Subnet> _listOfSubnets;
        private Models.Port _Port;
        private List<Models.Port> _listOfPorts;

        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "Network", Order: 1.2)]
        [AvailableValues("listOfNetworks")]
        public Network Network { get { return _Network; } set { ChangeNetwork(value); } }

        [Display(Name: "Subnet", Order: 1.3)]
        [AvailableValues("listOfSubnets")]
        public Subnet Subnet { get { return _Subnet; } set { ChangeSubnet(value); } }

        [Display(Name: "Port", Order: 1.4)]
        [AvailableValues("listOfPorts")]
        public Models.Port Port { get { return _Port; } set { _Port = value; } }

        #endregion
        [Display(Name: "List of Networks", Description: "Do not edit", Order: 2.1)]
        [Browsable(false)]
        public List<Network> listOfNetworks
        {
            get { return _listOfNetworks; }
            set
            {
                _listOfNetworks = value;
                OnPropertyChanged("ListOfNetworks");
            }
        }

        [Display(Name: "List of Subnets", Description: "Do not edit", Order: 2.2)]
        [Browsable(false)]
        public List<Subnet> listOfSubnets
        {
            get { return _listOfSubnets; }
            set
            {
                _listOfSubnets = value;
                OnPropertyChanged("ListOfSubnets");
            }
        }

        [Display(Name: "List of Ports", Description: "Do not edit", Order: 2.3)]
        [Browsable(false)]
        public List<Models.Port> listOfPorts
        {
            get { return _listOfPorts; }
            set
            {
                _listOfPorts = value;
                OnPropertyChanged("ListOfPorts");
            }
        }

        public AddPortToServerTestStep()
        {
            Neutron neutron = APIInstrument.GetNeutron();
            listOfNetworks = neutron.networks;
            Network = listOfNetworks.FirstOrDefault();
        }

        private void ChangeNetwork(Network newNetwork)
        {
            _Network = newNetwork;
            listOfSubnets = newNetwork.subnets;
            Subnet = newNetwork.subnets.FirstOrDefault();
        }

        private void ChangeSubnet(Subnet newSubnet)
        {
            _Subnet = newSubnet;
            listOfPorts = newSubnet.ports;
            Port = newSubnet.ports.FirstOrDefault();
        }

        public override void PrePlanRun()
        {  
            base.PrePlanRun();
            CreateServerTestStep parent = GetParent<CreateServerTestStep>();
            parent.AddPort(Port);
        }

        public override void Run()
        {
            UpgradeVerdict(OpenTap.Verdict.Pass);
        }

        public override void PostPlanRun()
        {
            base.PostPlanRun();
        }
    }
}
