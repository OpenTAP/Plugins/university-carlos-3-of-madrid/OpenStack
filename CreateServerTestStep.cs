//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;


namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack",  "Compute" }, Name: "New Server")]
    public class CreateServerTestStep : TestStep
    {
        private Server server = new Server("", null, null, new List<Models.Port>(), "");
        private List<Image> _listOfImages;
        private List<Flavor> _listOfFlavors;
        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "New Server Name", Order: 1.1)]
        public string ServerName { get { return server.name; } set { server.name = value; } }

        [Display("Image", Order: 1.2)]
        [AvailableValues("listOfImages")]
        public Image Image { get { return server.image; } set { server.image = value; } }

        [Display(Name: "Flavor", Order: 1.3)]
        [AvailableValues("listOfFlavors")]
        public Flavor Flavor { get { return server.flavor; } set { server.flavor = value; } }

        [Display("Add Port", Order: 1.4)]
        [Browsable(true)]
        public void AddPort()
        {
            AddPortToServerTestStep newPortChild = new AddPortToServerTestStep();
            ChildTestSteps.Add(newPortChild);
        }

        [Display(Name: "Check Instantiation", Description: "Add this server to Check Instantiation test step", Order: 1.5)]
        public bool Check { get; set; }

        [Display(Name: "Clean", Description: "Delete after finishing the test", Order: 1.6)]
        public bool Delete { get; set; }

        #endregion

        [Display(Name: "List of Images", Description: "Do not edit", Order: 2.1)]
        [Browsable(false)]
        public List<Image> listOfImages
        {
            get { return _listOfImages; }
            set
            {
                _listOfImages = value;
                OnPropertyChanged("ListOfImages");
            }
        }

        [Display(Name: "List of Flavors", Description: "Do not edit", Order: 2.2)]
        [Browsable(false)]
        public List<Flavor> listOfFlavors
        {
            get { return _listOfFlavors; }
            set
            {
                _listOfFlavors = value;
                OnPropertyChanged("ListOfFlavors");
            }
        }

        public CreateServerTestStep()
        {
            ServerName = "test";
            Check = true;
            Delete = true;
            Nova nova = APIInstrument.GetNova();
            listOfImages = nova.images;
            Image = listOfImages.FirstOrDefault();
            listOfFlavors = nova.flavors;
            Flavor = listOfFlavors.FirstOrDefault();
        }

        public void AddPort(Models.Port port)
        {
            server.ports.Add(port);
        }

        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void Run()
        {
            RunChildSteps();
            UpgradeVerdict(APIInstrument.CreateServer(server, Check));
        }

        public override void PostPlanRun()
        {
            if (Delete && server.id != "") APIInstrument.DeleteServer(server);
            base.PostPlanRun();
        }
    }
}
