//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack", "API" }, Name: "OpenStack API", Description: "OpenStack API")]    

    public class APIInstrument : Instrument
    {
        [Display(Name: "Host", Order: 1.1)]
        public string DefaultHost { get; set; }

        [Display(Name: "Port", Order: 1.2)]
        public int DefaultPort { get; set; }

        [Display(Name: "Domain", Order: 1.3)]
        public string Domain { get; set; }

        [Display(Group: "Credentials", Name: "User", Order: 2.1)]
        public string User { get; set; }

        [Display(Group: "Credentials", Name: "Password", Order: 2.2)]
        public string Password { get; set; }

        [Display(Group: "Identity", Name: "Default", Order: 3.1)]
        public bool IdentityDefault { get; set; }

        [Display(Group: "Identity", Name: "Host", Order: 3.2)]
        [EnabledIf("IdentityDefault", false, HideIfDisabled = true)]
        public string IdentityHost { get; set; }

        [Display(Group: "Identity", Name: "Port", Order: 3.3)]
        [EnabledIf("IdentityDefault", false, HideIfDisabled = true)]
        public int IdentityPort { get; set; }

        [Display(Group: "Network", Name: "Default", Order: 4.1)]
        public bool NetworkDefault { get; set; }

        [Display(Group: "Network", Name: "Host", Order: 4.2)]
        [EnabledIf("NetworkDefault", false, HideIfDisabled = true)]
        public string NetworkHost { get; set; }

        [Display(Group: "Network", Name: "Port", Order: 4.3)]
        [EnabledIf("NetworkDefault", false, HideIfDisabled = true)]
        public int NetworkPort { get; set; }

        [Display(Group: "Compute", Name: "Default", Order: 5.1)]
        public bool ComputeDefault { get; set; }

        [Display(Group: "Compute", Name: "Host", Order: 5.2)]
        [EnabledIf("ComputeDefault", false, HideIfDisabled = true)]
        public string ComputeHost { get; set; }

        [Display(Group: "Compute", Name: "Port", Order: 5.3)]
        [EnabledIf("ComputeDefault", false, HideIfDisabled = true)]
        public int ComputePort { get; set; }

        [Display(Group: "Image", Name: "Default", Order: 6.1)]
        public bool ImageDefault { get; set; }

        [Display(Group: "Image", Name: "Host", Order: 6.2)]
        [EnabledIf("ImageDefault", false, HideIfDisabled = true)]
        public string ImageHost { get; set; }

        [Display(Group: "Image", Name: "Port", Order: 6.3)]
        [EnabledIf("ImageDefault", false, HideIfDisabled = true)]
        public int ImagePort { get; set; }

        private HttpClient client;
        private string clientError;
        private string AuthToken;
        private bool isInit;
        private bool loadNeutron;
        private bool loadNova;

        private Neutron neutron = new Neutron();
        private Nova nova = new Nova();

        public APIInstrument()
        {
            Name = "API";
            
            DefaultHost = "10.5.10.4";
            DefaultPort = 5000;
            Domain = "5G-VINNI-DEV";

            User = "";
            Password = "";
            

            IdentityDefault = false;
            IdentityPort = 5000;

            NetworkDefault = false;
            NetworkHost = "10.5.10.4";
            NetworkPort = 9696;

            ComputeDefault = false;
            ComputeHost = "10.5.10.4";
            ComputePort = 8774;

            ImageDefault = false;
            ImageHost = "10.5.10.4";
            ImagePort = 9292;

            isInit = false;
            loadNeutron = true;
            loadNova = true;
        }

        // Open procedure for the instrument.
        public override void Open()
        {
            init();
            base.Open();
        }

        public void init()
        {
            if (isInit) return;
            IdentityHost = IdentityDefault ? DefaultHost : IdentityHost;
            IdentityPort = IdentityDefault ? DefaultPort : IdentityPort;

            NetworkHost = NetworkDefault ? DefaultHost : NetworkHost;
            NetworkPort = NetworkDefault ? DefaultPort : NetworkPort;

            ComputeHost = ComputeDefault ? DefaultHost : ComputeHost;
            ComputePort = ComputeDefault ? DefaultPort : ComputePort;

            ImageHost = ImageDefault ? DefaultHost : ImageHost;
            ImagePort = ImageDefault ? DefaultPort : ImagePort;

            client = new HttpClient();
            Auth();
            if (client == null) Log.Info(clientError);
            isInit = true;
        }
       
        // Close procedure for the instrument.
        public override void Close()
        {
            if (client != null) client.Dispose();
            isInit = false;
            base.Close();   
        }

        public void Auth()
        {
            string requestUri = "http://" + IdentityHost + ":" + IdentityPort.ToString() + "/identity/v3/auth/tokens";

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var json = new Dictionary<string, object>();
            var auth = new Dictionary<string, object>();
            var identity = new Dictionary<string, object>();
            var methods = new List<string>();
            methods.Add("password");
            identity.Add("methods", methods);
            var password = new Dictionary<string, object>();
            var user = new Dictionary<string, object>();
            user.Add("name", User);
            var domain = new Dictionary<string, object>();
            domain.Add("name", Domain);
            user.Add("domain", domain);
            user.Add("password", Password);
            password.Add("user", user);
            identity.Add("password", password);
            auth.Add("identity", identity);
            json.Add("auth", auth);
            
            HttpResponseMessage response = client.PostAsync(requestUri, new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json")).Result;
            if (!response.IsSuccessStatusCode)
            {
                clientError = string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                client = null;
                return;
            }

            AuthToken = response.Headers.GetValues("X-Subject-Token").FirstOrDefault();
            Log.Info("Auth completed. Token: {0}", AuthToken);
            client.DefaultRequestHeaders.Add("X-Auth-Token", AuthToken);
            return;
        }

        public OpenTap.Verdict CreateNetwork(Network network)
        {
            if (client == null) return OpenTap.Verdict.Error;
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/networks";
            var json = new Dictionary<string, object>();
            var networkJson = new Dictionary<string, object>();
            networkJson.Add("name", network.name);
            json.Add("network", networkJson);
            HttpResponseMessage response = client.PostAsync(requestUri, new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json")).Result;
            string rstring = response.Content.ReadAsStringAsync().Result;
            JObject rjson = JObject.Parse(rstring);
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("{0} ({1}). {2} ", (int)response.StatusCode, response.ReasonPhrase, rstring);
                return OpenTap.Verdict.Error;
            }
            network.id = rjson["network"]["id"].ToString();
            Log.Info("Network {0} created. ID: {1}", network.name, network.id);
            return OpenTap.Verdict.Pass;
        }

        public bool DeleteNetwork(Network network)
        {
            if (client == null) return false;
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/networks/" + network.id;
            HttpResponseMessage response = client.DeleteAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Network {0}: {1} ({2})", network.name, (int)response.StatusCode, response.ReasonPhrase);
                return false;
            }
            Log.Info("Network {0} deleted", network.name);
            neutron.networks.Remove(network);
            return true;
        }

        public OpenTap.Verdict CreateSubnet(Network network, Subnet subnet)
        {
            if (client == null) return OpenTap.Verdict.Error;

            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/subnets";
            var json = new Dictionary<string, object>();
            var subnetJson = new Dictionary<string, object>();
            subnetJson.Add("network_id", network.id);
            subnetJson.Add("name", subnet.name);
            subnetJson.Add("ip_version", subnet.ipVersion);
            subnetJson.Add("cidr", subnet.CIDR);
            subnetJson.Add("enable_dhcp", subnet.dhcp);
            json.Add("subnet", subnetJson);
            HttpResponseMessage response = client.PostAsync(requestUri, new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json")).Result;
            string rstring = response.Content.ReadAsStringAsync().Result;
            JObject rjson = JObject.Parse(rstring);
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("{0} ({1}). {2}", (int)response.StatusCode, response.ReasonPhrase, rstring);
                return OpenTap.Verdict.Error;
            }

            subnet.id = rjson["subnet"]["id"].ToString();
            
            Log.Info("Subnet {0} created. ID: {1}", subnet.name, subnet.id);
            return OpenTap.Verdict.Pass;
        }

        public bool DeleteSubnet(Network network, Subnet subnet)
        {
            if (client == null) return false;
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/subnets/" + subnet.id;
            HttpResponseMessage response = client.DeleteAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Subnet {0}: {1} ({2})", subnet.name, (int)response.StatusCode, response.ReasonPhrase);
                return false;
            }
            network.DeleteSubnet(subnet);
            Log.Info("Subnet {0} deleted", network.name);
            return true;
        }

        public OpenTap.Verdict CreatePort(Network network, Subnet subnet, Models.Port port)
        {
            if (client == null) return OpenTap.Verdict.Error;

            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/ports";
            var json = new Dictionary<string, object>();
            var portJson = new Dictionary<string, object>();
            portJson.Add("network_id", network.id);
            portJson.Add("name", port.name);
            if (!string.IsNullOrEmpty(port.securityGroup.name))
            {
                portJson.Add("security_groups", new List<string>() { port.securityGroup.id });
            }
            if (!string.IsNullOrEmpty(port.ip))
            {
                var f_ip = new Dictionary<string, string>() { { "ip_address", port.ip }, { "subnet_id", subnet.id } };
                var f_ips = new List<Dictionary<string, string>>() { f_ip };
                portJson.Add("fixed_ips", f_ips);
            }
            json.Add("port", portJson);
            HttpResponseMessage response = client.PostAsync(requestUri, new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json")).Result;
            string rstring = response.Content.ReadAsStringAsync().Result;
            JObject rjson = JObject.Parse(rstring);
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("{0} ({1}). {2}", (int)response.StatusCode, response.ReasonPhrase, rstring);
                return OpenTap.Verdict.Error;
            }

            port.id = rjson["port"]["id"].ToString();
            Log.Info("Port {0} created. ID: {1}", port.name, port.id);
            return OpenTap.Verdict.Pass;
        }

        public bool DeletePort(Subnet subnet, Models.Port port)
        {
            if (client == null) return false;
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/ports/" + port.id;
            HttpResponseMessage response = client.DeleteAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Port {0}: {1} ({2})", port.name, (int)response.StatusCode, response.ReasonPhrase);
                return false;
            }
            Log.Info("Port {0} deleted", port.name);
            subnet.DeletePort(port);
            return true;
        }

        public OpenTap.Verdict CreateServer(Server server, bool check)
        {
            if (client == null) return OpenTap.Verdict.Error;

            if (server.ports.Count() < 1)
            {
                Log.Info("At least one port is required");
                return OpenTap.Verdict.Error;
            }

            string requestUri = "http://" + ComputeHost + ":" + ComputePort + "/v2.1/servers";
            var json = new Dictionary<string, object>();
            var serverJson = new Dictionary<string, object>();
            serverJson.Add("name", server.name);
            serverJson.Add("imageRef", server.image.id);
            serverJson.Add("flavorRef", server.flavor.id);
            var networks = new List<object>();
            foreach (var port in server.ports)
            {
                if (port == null) return OpenTap.Verdict.Error;
                networks.Add(new Dictionary<string, string>() { { "port", port.id } });
            }
            serverJson.Add("networks", networks);
            json.Add("server", serverJson);
            HttpResponseMessage response = client.PostAsync(requestUri, new StringContent(JsonConvert.SerializeObject(json), System.Text.Encoding.UTF8, "application/json")).Result;
            string rstring = response.Content.ReadAsStringAsync().Result;
            JObject rjson = JObject.Parse(rstring);
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("{0} ({1}). {2}", (int)response.StatusCode, response.ReasonPhrase, rstring);
                return OpenTap.Verdict.Error;
            }

            server.id = rjson["server"]["id"].ToString();
            if (check) nova.serversCheckList.Add(server);
            Log.Info("Server {0} created. ID: {1}", server.name, server.id);
            return OpenTap.Verdict.Pass;
        }

        public bool DeleteServer(Server server)
        {
            if (client == null) return false;
            string requestUri = "http://" + ComputeHost + ":" + ComputePort + "/v2.1/servers/" + server.id;
            HttpResponseMessage response = client.DeleteAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Server {0}: {1} ({2})", server.name, (int)response.StatusCode, response.ReasonPhrase);
                return false;
            }
            Log.Info("Server {0} deleted", server.name);
            nova.DeleteServer(server);
            return true;
        }

        public OpenTap.Verdict CheckServers()
        {
            if (client == null) return OpenTap.Verdict.Error;

            while (nova.serversCheckList.Count() > 0)
            {
                foreach (Server server in nova.serversCheckList)
                {
                    string requestUri = "http://" + ComputeHost + ":" + ComputePort + "/v2.1/servers/" + server.id;
                    HttpResponseMessage response = client.GetAsync(requestUri).Result;
                    string rstring = response.Content.ReadAsStringAsync().Result;
                    JObject rjson = JObject.Parse(rstring);
                    if (!response.IsSuccessStatusCode)
                    {
                        Log.Info("{0} ({1}). {2}", (int)response.StatusCode, response.ReasonPhrase, rstring);
                        return OpenTap.Verdict.Error;
                    }

                    string status = rjson["server"]["status"].ToString();
                    if (status == "ACTIVE")
                    {
                        Log.Info("Server {0} status: {1}", server, status);
                        nova.serversCheckList.Remove(server);
                        break;
                    }
                    else if (status != "BUILD")
                    {
                        Log.Error("Server {0} status: {1}", server, status);
                        return OpenTap.Verdict.Error;
                    }
                }
            }
            return OpenTap.Verdict.Pass;
        }

        public Neutron GetNeutron()
        {
            init();
            if (!loadNeutron) return neutron;
            LoadNetworks();
            LoadSecurityGroups();
            loadNeutron = false;
            return neutron;
        }

        private void LoadNetworks()
        {
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/networks";
            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Get ID: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return;
            }
            string data = response.Content.ReadAsStringAsync().Result;

            try
            {
                var networks = JObject.Parse(data)["networks"];
                foreach (var network in networks)
                {
                    Network newNetwork = new Network(network["name"].ToString(), network["id"].ToString());
                    neutron.networks.Add(newNetwork);
                    LoadSubnets(newNetwork, network["subnets"]);
                }
            }
            catch (Exception)
            {
                Log.Info("Get networks request error");
            }
        }

        private void LoadSubnets(Network network, JToken subnets)
        {   
            foreach (var subnet in subnets)
            {
                string subnetId = subnet.ToString();
                string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/subnets?id=" + subnetId + "&network_id=" +network.id;
                HttpResponseMessage response = client.GetAsync(requestUri).Result;
                if (!response.IsSuccessStatusCode)
                {
                    Log.Info("Get Subnets: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    return;
                }
                string data = response.Content.ReadAsStringAsync().Result;

                try
                {
                    var name = JObject.Parse(data)["subnets"][0]["name"].ToString();
                    var dhcp = Convert.ToBoolean(JObject.Parse(data)["subnets"][0]["enable_dhcp"]);
                    var CIDR = JObject.Parse(data)["subnets"][0]["cidr"].ToString();
                    Subnet newSubnet = new Subnet(name, 4, CIDR, dhcp, subnetId);
                    LoadPorts(network, newSubnet);
                    network.AddSubnet(newSubnet);
                }
                catch (Exception)
                {
                    Log.Info("{0} ({1}) does not exist", subnet, network.name);
                    return;
                }
            }        
        }

        private void LoadPorts(Network network, Subnet subnet)
        {
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/ports?network_id=" + network.id;
            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Get Ports: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return;
            }
            string data = response.Content.ReadAsStringAsync().Result;

            try
            {
                var ports = JObject.Parse(data)["ports"];
                foreach (var port in ports)
                {
                    if (port["fixed_ips"][0]["subnet_id"].ToString() == subnet.id)
                    {
                        string portName = port["name"].ToString();
                        string portIp = port["fixed_ips"][0]["ip_address"].ToString();
                        string portId = port["id"].ToString();
                        // TODO fill security group
                        Models.Port newPort = new Models.Port(portName, portIp, null, portId);
                        subnet.AddPort(newPort);
                    }   
                }
            }
            catch (Exception)
            {
                Log.Info("Get ports request error");
            }
        }

        private void LoadSecurityGroups()
        {
            string requestUri = "http://" + NetworkHost + ":" + NetworkPort + "/v2.0/security-groups";
            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Get ID: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return;
            }
            string data = response.Content.ReadAsStringAsync().Result;

            try
            {
                var securityGroups = JObject.Parse(data)["security_groups"];
                foreach (var securityGroup in securityGroups)
                {
                    SecurityGroup newSecurityGroup = new SecurityGroup(securityGroup["name"].ToString(), securityGroup["id"].ToString());
                    neutron.securityGroups.Add(newSecurityGroup);
                }
            }
            catch (Exception)
            {
                Log.Info("Get security groups request error");
                return;
            }
        }

        public Nova GetNova()
        {
            init();
            if (!loadNova) return nova;
            LoadImages();
            LoadFlavors();
            LoadServers();
            loadNova = false;
            return nova;
        }

        private void LoadImages()
        {
            string requestUri = "http://" + ImageHost + ":" + ImagePort + "/v2/images";
            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Get Image ID: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return;
            }
            string data = response.Content.ReadAsStringAsync().Result;

            try
            {
                var images = JObject.Parse(data)["images"];
                foreach (var image in images)
                {
                    Image newImage = new Image(image["name"].ToString(), image["id"].ToString());
                    nova.images.Add(newImage);
                }
            }
            catch (Exception)
            {
                Log.Info("Get images request error");
            }
        }

        private void LoadFlavors()
        {
            string requestUri = "http://" + ComputeHost + ":" + ComputePort + "/v2.1/flavors";
            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Get Flavor ID: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return;
            }
            string data = response.Content.ReadAsStringAsync().Result;

            try
            {
                var flavors = JObject.Parse(data)["flavors"];
                foreach (var flavor in flavors)
                {
                    Flavor newFlavor = new Flavor(flavor["name"].ToString(), flavor["id"].ToString());
                    nova.flavors.Add(newFlavor);
                }
            }
            catch (Exception)
            {
                Log.Info("Get flavors request error");
            }
        }

        private void LoadServers()
        {
            string requestUri = "http://" + ComputeHost + ":" + ComputePort + "/v2.1/servers";
            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            if (!response.IsSuccessStatusCode)
            {
                Log.Info("Get Flavor ID: {0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return;
            }
            string data = response.Content.ReadAsStringAsync().Result;

            try
            {
                var servers = JObject.Parse(data)["servers"];
                foreach (var server in servers)
                {
                    string name = server["name"].ToString();
                    string id = server["id"].ToString();
                    Server newServer = new Server(name, null, null, null, id);
                    nova.servers.Add(newServer);
                }
            }
            catch (Exception)
            {
                Log.Info("Get servers request error");
            }
        }
    }
}
