//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Xml.Serialization;

namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack",  "Network" }, Name: "New Subnet")]
    public class CreateSubnetTestStep : TestStep
    {
        private Subnet subnet = new Subnet("", 4, "", true, "");
        private Network _Network;
        private List<Network> _listOfNetworks;
        private List<CreatePortAsChildTestStep> portChilds = new List<CreatePortAsChildTestStep>();

        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "New Subnet Name", Order: 1.0)]
        public string SubnetName { get { return subnet.name; } set { subnet.name = value; } }

        [Display("Network", Order: 1.1)]
        [AvailableValues("listOfNetworks")]
        public Network Network { get { return _Network; } set { ChangeNetwork(value); } }

        [Display(Name: "CIDR", Order: 1.2)]
        public string CIDR { get { return subnet.CIDR; } set { subnet.CIDR = value; } }

        [Display(Name: "Enable DHCP", Order: 1.3)]
        public bool DHCP { get { return subnet.dhcp; } set { subnet.dhcp = value; } }

        [Display(Name: "Clean", Description: "Delete after finishing the test", Order: 1.4)]
        public bool Delete { get; set; }

        [Display("Add Port", Order: 2.1)]
        [Browsable(true)]
        public void AddPort()
        {
            CreatePortAsChildTestStep newPortChild = new CreatePortAsChildTestStep(Network, subnet);
            portChilds.Add(newPortChild);
            ChildTestSteps.Add(newPortChild);
        }

        #endregion

        [Display(Name: "List of Networks", Description: "Do not edit", Order: 2.1)]
        [Browsable(false)]
        public List<Network> listOfNetworks
        {
            get { return _listOfNetworks; }
            set
            {
                _listOfNetworks = value;
                OnPropertyChanged("ListOfNetworks");
            }
        }

        public CreateSubnetTestStep()
        {
            SubnetName = "testSN1";
            CIDR = "10.10.1.0/24";
            DHCP = true;
            Delete = true;
            listOfNetworks = APIInstrument.GetNeutron().networks;
            Network = listOfNetworks.FirstOrDefault();
            _Network.AddSubnet(subnet);
        }

        private void ChangeNetwork(Network newNetwork)
        {
            try
            {
                _Network.DeleteSubnet(subnet);
            }
            catch
            { }
            
            _Network = newNetwork;
            Network.AddSubnet(subnet);
            foreach (CreatePortAsChildTestStep child in portChilds)
            {
                child.ChangeNetwork(newNetwork);
            }
        }
        
        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void Run()
        {
            UpgradeVerdict(APIInstrument.CreateSubnet(Network, subnet));
            RunChildSteps();
        }

        public override void PostPlanRun()
        {
            if (Delete) APIInstrument.DeleteSubnet(Network, subnet);
            base.PostPlanRun();
        }
    }
}
