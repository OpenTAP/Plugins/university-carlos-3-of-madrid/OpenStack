//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.ComponentModel;

namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack", "Network" }, Name: "New Network")]
    public class CreateNetworkTestStep : TestStep
    {
        private Network network;
        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "Network Name", Order: 1.1)]
        public string NetworkName { get { return network.name; } set { network.name = value; } }

        [Display(Name: "Clean", Description: "Delete after finishing the test", Order: 1.2)]
        public bool Delete { get; set; }

        [Display("Add Subnet", Order: 2.1)]
        [Browsable(true)]
        public void AddSubnet()
        {
            ChildTestSteps.Add(new CreateSubnetAsChildTestStep(network));
        }

        #endregion

        public CreateNetworkTestStep()
        {
            network = new Network("", "");
            NetworkName = "testN1";
            Delete = true;
            APIInstrument.GetNeutron().networks.Add(network);
        }

        public Network GetNetwork()
        {
            return network;
        }

        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void Run()
        {
            UpgradeVerdict(APIInstrument.CreateNetwork(network));
            try
            {
                RunChildSteps();
            }
            catch { }
            
        }

        public override void PostPlanRun()
        {
            if (Delete) APIInstrument.DeleteNetwork(network);
            base.PostPlanRun();
        }
    }
}
