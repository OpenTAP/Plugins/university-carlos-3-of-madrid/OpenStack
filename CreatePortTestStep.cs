//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using System.ComponentModel;

namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack",  "Network" }, Name: "New Port")]
    public class CreatePortTestStep : TestStep
    {
        private Models.Port port = new Models.Port("", "", null, "");

        private Network _Network;
        private List<Network> _listOfNetworks;
        private Subnet _Subnet;
        private List<Subnet> _listOfSubnets;
        private SecurityGroup _SecurityGroup;
        private List<SecurityGroup> _listOfSecurityGroups;

        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "Network", Order: 1.2)]
        [AvailableValues("listOfNetworks")]
        public Network Network { get { return _Network; } set { ChangeNetwork(value); } }

        [Display(Name: "Subnet", Order: 1.3)]
        [AvailableValues("listOfSubnets")]
        public Subnet Subnet { get { return _Subnet; } set { ChangeSubnet(value); } }

        [Display(Name: "New Port Name", Order: 1.1)]
        public string PortName { get { return port.name; } set { port.name = value; } }
        [Display(Name: "IP", Order: 1.4)]
        public string IP { get { return port.ip; } set { port.ip = value; } }

        [Display(Name: "Security Group Name", Order: 1.5)]
        [AvailableValues("listOfSecurityGroups")]
        public SecurityGroup SecurityGroup { get { return _SecurityGroup; } set { _SecurityGroup = value; port.securityGroup = value; } }

        [Display(Name: "Clean", Description: "Delete after finishing the test", Order: 1.6)]
        public bool Delete { get; set; }

        #endregion

        [Display(Name: "List of Networks", Description: "Do not edit", Order: 2.1)]
        [Browsable(false)]
        public List<Network> listOfNetworks
        {
            get { return _listOfNetworks; }
            set
            {
                _listOfNetworks = value;
                OnPropertyChanged("ListOfNetworks");
            }
        }

        [Display(Name: "List of Subnets", Description: "Do not edit", Order: 2.2)]
        [Browsable(false)]
        public List<Subnet> listOfSubnets
        {
            get { return _listOfSubnets; }
            set
            {
                _listOfSubnets = value;
                OnPropertyChanged("ListOfSubnets");
            }
        }

        [Display(Name: "List of Security Groups", Description: "Do not edit", Order: 2.3)]
        [Browsable(false)]
        public List<SecurityGroup> listOfSecurityGroups
        {
            get { return _listOfSecurityGroups; }
            set
            {
                _listOfSecurityGroups = value;
                OnPropertyChanged("ListOfSecurityGroups");
            }
        }

        public CreatePortTestStep()
        {
            PortName = "testP1";
            IP = "10.10.1.1";
            Delete = true;
            Neutron neutron = APIInstrument.GetNeutron();
            listOfNetworks = neutron.networks;
            Network = listOfNetworks.FirstOrDefault();
            listOfSecurityGroups = neutron.securityGroups;
            SecurityGroup = listOfSecurityGroups.FirstOrDefault();
        }

        private void ChangeNetwork(Network newNetwork)
        {
            _Network = newNetwork;
            listOfSubnets = newNetwork.subnets;
            Subnet = newNetwork.subnets.FirstOrDefault();
        }

        private void ChangeSubnet(Subnet newSubnet)
        {
            try
            {
                _Subnet.DeletePort(port);
            }
            catch (Exception)
            { }

            try
            {
                _Subnet = newSubnet;
                newSubnet.AddPort(port);
            }
            catch (Exception)
            { }     
        }

        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void Run()
        {
            UpgradeVerdict(APIInstrument.CreatePort(Network, Subnet, port));
        }

        public override void PostPlanRun()
        {
            if (Delete) APIInstrument.DeletePort(Subnet, port);
            base.PostPlanRun();
        }
    }
}
