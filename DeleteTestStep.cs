//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack" }, Name: "Delete Item")]
    public class DeleteTestStep : TestStep
    {
        private List<string> _listOfTypes = new List<string> { "Network", "Subnet", "Port", "Server" };
        private Network _Network;
        private List<Network> _listOfNetworks;
        private Subnet _Subnet;
        private List<Subnet> _listOfSubnets;
        private Models.Port _Port;
        private List<Models.Port> _listOfPorts;
        private Server _Server;
        private List<Server> _listOfServers;

        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "Type", Order: 1.1)]
        [AvailableValues("listOfTypes")]
        public string Type { get; set; }

        [Display(Name: "Network", Order: 1.2)]
        [AvailableValues("listOfNetworks")]
        [EnabledIf("EnableNetwork", true, HideIfDisabled = true)]
        public Network Network { get { return _Network; } set { ChangeNetwork(value); } }

        [Display(Name: "Subnet", Order: 1.3)]
        [AvailableValues("listOfSubnets")]
        [EnabledIf("EnableSubnet", true, HideIfDisabled = true)]
        public Subnet Subnet { get { return _Subnet; } set { ChangeSubnet(value); } }

        [Display(Name: "Port", Order: 1.4)]
        [AvailableValues("listOfPorts")]
        [EnabledIf("EnablePort", true, HideIfDisabled = true)]
        public Models.Port Port { get { return _Port; } set { _Port = value; } }

        [Display(Name: "Server", Order: 1.4)]
        [AvailableValues("listOfServers")]
        [EnabledIf("EnableServer", true, HideIfDisabled = true)]
        public Server Server { get { return _Server; } set { _Server = value; } }

        #endregion
        [Display(Name: "List of Types", Description: "Do not edit", Order: 2.1)]
        [Browsable(false)]
        public List<string> listOfTypes
        {
            get { return _listOfTypes; }
            set
            {
                _listOfTypes = value;
                OnPropertyChanged("ListOfNetworks");
            }
        }

        [Display(Name: "List of Networks", Description: "Do not edit", Order: 2.1)]
        [Browsable(false)]
        public List<Network> listOfNetworks
        {
            get { return _listOfNetworks; }
            set
            {
                _listOfNetworks = value;
                OnPropertyChanged("ListOfNetworks");
            }
        }

        [Display(Name: "List of Subnets", Description: "Do not edit", Order: 2.2)]
        [Browsable(false)]
        public List<Subnet> listOfSubnets
        {
            get { return _listOfSubnets; }
            set
            {
                _listOfSubnets = value;
                OnPropertyChanged("ListOfSubnets");
            }
        }

        [Display(Name: "List of Ports", Description: "Do not edit", Order: 2.3)]
        [Browsable(false)]
        public List<Models.Port> listOfPorts
        {
            get { return _listOfPorts; }
            set
            {
                _listOfPorts = value;
                OnPropertyChanged("ListOfPorts");
            }
        }

        [Display(Name: "List of Servers", Description: "Do not edit", Order: 2.4)]
        [Browsable(false)]
        public List<Server> listOfServers
        {
            get { return _listOfServers; }
            set
            {
                _listOfServers = value;
                OnPropertyChanged("ListOfServers");
            }
        }

        public bool EnableNetwork { get { return (Type == "Network" || Type == "Subnet" || Type == "Port"); }}
        public bool EnableSubnet { get { return (Type == "Subnet" || Type == "Port"); } }
        public bool EnablePort { get { return Type == "Port"; } }
        public bool EnableServer { get { return Type == "Server"; } }

        public DeleteTestStep()
        {
            Neutron neutron = APIInstrument.GetNeutron();
            Nova nova = APIInstrument.GetNova();
            listOfNetworks = neutron.networks;
            Network = listOfNetworks.FirstOrDefault();
            listOfServers = nova.servers;
            Server = listOfServers.FirstOrDefault();
        }

        private void ChangeNetwork(Network newNetwork)
        {
            _Network = newNetwork;
            listOfSubnets = newNetwork.subnets;
            Subnet = newNetwork.subnets.FirstOrDefault();
        }

        private void ChangeSubnet(Subnet newSubnet)
        {
            _Subnet = newSubnet;
            listOfPorts = newSubnet.ports;
            Port = newSubnet.ports.FirstOrDefault();
        }

        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void Run()
        {
            switch(Type)
            {
                case "Network":
                    DeleteNetwork(Network);
                    break;
                case "Subnet":
                    DeleteSubnet(Network, Subnet);
                    break;
                case "Port":
                    DeletePort(Subnet, Port);
                    break;
                case "Server":
                    DeleteServer(Server);
                    break;
            }
        }

        public bool DeleteNetwork(Network network)
        {
            bool result;
            while (network.subnets.Count > 0)
            {
                result = DeleteSubnet(network, network.subnets.First());
                if (!result)
                {
                    UpgradeVerdict(OpenTap.Verdict.Error);
                    return false;
                }
            }
            result = APIInstrument.DeleteNetwork(network);
            if (!result)
            {
                UpgradeVerdict(OpenTap.Verdict.Error);
                return false;
            }
            UpgradeVerdict(OpenTap.Verdict.Pass);
            return true;
        }

        public bool DeleteSubnet(Network network, Subnet subnet)
        {
            bool result;
            while (subnet.ports.Count > 0)
            {
                result = DeletePort(subnet, subnet.ports.First());
                if (!result)
                {
                    UpgradeVerdict(OpenTap.Verdict.Error);
                    return false;
                }
            }
            result = APIInstrument.DeleteSubnet(network, subnet);
            if (!result)
            {
                UpgradeVerdict(OpenTap.Verdict.Error);
                return false;
            }
            UpgradeVerdict(OpenTap.Verdict.Pass);
            return true;
        }

        public bool DeletePort(Subnet subnet, Models.Port port)
        {
            bool result = APIInstrument.DeletePort(subnet, port);
            if (!result)
            {
                UpgradeVerdict(OpenTap.Verdict.Error);
                return false;
            }
            UpgradeVerdict(OpenTap.Verdict.Pass);
            return true;
        }

        public bool DeleteServer(Server server)
        {
            bool result = APIInstrument.DeleteServer(server);
            if (!result)
            {
                UpgradeVerdict(OpenTap.Verdict.Error);
                return false;
            }
            UpgradeVerdict(OpenTap.Verdict.Pass);
            return true;
        }

        public override void PostPlanRun()
        {
            base.PostPlanRun();
        }
    }
}
