//Copyright 2012-2019 Keysight Technologies
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.
using OpenTap.Plugins.OpenStackPlugin.Models;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Xml.Serialization;

namespace OpenTap.Plugins.OpenStackPlugin
{
    [Display(Groups: new[] { "OpenStack",  "Network" }, Name: "New Subnet")]
    [AllowAsChildIn(typeof(CreateNetworkTestStep))]
    [Browsable(false)]
    public class CreateSubnetAsChildTestStep : TestStep
    {
        private Subnet subnet = new Subnet("", 4, "", true, "");
        private Network Network;

        #region Settings
        [Display(Name: "API Instrument", Order: 1.0)]
        public APIInstrument APIInstrument { get; set; }

        [Display(Name: "New Subnet Name", Order: 1.0)]
        public string SubnetName { get { return subnet.name; } set { subnet.name = value; } }

        [Display(Name: "CIDR", Order: 1.2)]
        public string CIDR { get { return subnet.CIDR; } set { subnet.CIDR = value; } }

        [Display(Name: "Enable DHCP", Order: 1.3)]
        public bool DHCP { get { return subnet.dhcp; } set { subnet.dhcp = value; } }

        [Display(Name: "Clean", Description: "Delete after finishing the test", Order: 1.4)]
        public bool Delete { get; set; }

        [Display("Add Port", Order: 2.1)]
        [Browsable(true)]
        public void AddPort()
        {
            ChildTestSteps.Add(new CreatePortAsChildTestStep(Network, subnet));
        }
        #endregion

        public CreateSubnetAsChildTestStep(Network network)
        {
            Network = network;
            Network.AddSubnet(subnet);
            SubnetName = "testSN1";
            CIDR = "10.10.1.0/24";
            DHCP = true;
            Delete = true;
        }

        public override void PrePlanRun()
        {
            base.PrePlanRun();
        }

        public override void Run()
        {
            UpgradeVerdict(APIInstrument.CreateSubnet(Network, subnet));
            try
            {
                RunChildSteps();
            }
            catch { }
            
        }

        public override void PostPlanRun()
        {
            if (Delete) APIInstrument.DeleteSubnet(Network, subnet);
            base.PostPlanRun();
        }
    }
}
