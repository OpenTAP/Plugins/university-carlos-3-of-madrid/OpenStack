﻿namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Flavor
    {
        public string name { get; set; }
        public string id { get; set; }

        public Flavor(string name, string id)
        {
            this.name = name;
            this.id = id;
        }
        public override string ToString()
        {
            return name;
        }
    }
}
