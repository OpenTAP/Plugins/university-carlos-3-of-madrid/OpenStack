﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Image
    {
        public string name { get; set; }
        public string id { get; set; }


        public Image(string name, string id)
        {
            this.name = name;
            this.id = id;
        }
        public override string ToString()
        {
            return name;
        }
    }
}
