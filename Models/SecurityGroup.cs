﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class SecurityGroup
    {
        public string name { get; }
        public string id { get; }


        public SecurityGroup(string name, string id)
        {
            this.name = name;
            this.id = id;
        }

        public override string ToString()
        {
            return name;
        }

    }
}
