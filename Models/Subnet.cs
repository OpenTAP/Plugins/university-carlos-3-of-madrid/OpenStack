﻿using System;
using System.Collections.Generic;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Subnet
    {
        public string name { get; set; }
        public int ipVersion { get; set; }
        public string CIDR { get; set; }
        public Boolean dhcp { get; set; }
        public string id { get; set; }

        public List<Port> ports { get; set; }

        public Subnet(string name, int ipVersion, string CIDR, Boolean dhcp, string id)
        {
            this.name = name;
            this.ipVersion = ipVersion;
            this.CIDR = CIDR;
            this.dhcp = dhcp;
            this.id = id;
            ports = new List<Port>();
        }

        public void AddPort(Port port)
        {
            ports.Add(port);
        }

        public void DeletePort(Port port)
        {
            ports.Remove(port);
        }

        public override string ToString()
        {
            return name;
        }
    }
}
