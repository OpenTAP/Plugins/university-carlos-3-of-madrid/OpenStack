﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Port
    {
        public string name { get; set; }
        public string ip { get; set; }
        public SecurityGroup securityGroup { get; set; }
        public string id { get; set; }


        public Port(string name, string ip, SecurityGroup securityGroup, string id)
        {
            this.name = name;
            this.ip = ip;
            this.securityGroup = securityGroup;
            this.id = id;
        }
        public override string ToString()
        {
            return name;
        }
    }
}
