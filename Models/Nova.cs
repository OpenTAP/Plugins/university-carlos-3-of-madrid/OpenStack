﻿using System.Collections.Generic;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Nova
    {
        public List<Server> servers;
        public List<Image> images;
        public List<Flavor> flavors;
        public List<Server> serversCheckList;
        public Nova()
        {
            servers = new List<Server>();
            images = new List<Image>();
            flavors = new List<Flavor>();
            serversCheckList = new List<Server>();
        }

        public Server AddServer(string name, Image image, Flavor flavor, List<Port> ports, string id)
        {
            Server server = new Server(name, image, flavor, ports, id);
            servers.Add(server);
            return server;
        }

        public void DeleteServer(Server server)
        {
            servers.Remove(server);
        }

        public Server GetServer(string serverName)
        {
            foreach (Server server in servers)
            {
                if (server.name == serverName) return server;
            }
            return null;
        }

        public Image GetImage(string id)
        {
            foreach (Image image in images)
            {
                if (image.id == id) return image;
            }
            return null;
        }

        public Flavor GetFlavor(string id)
        {
            foreach (Flavor flavor in flavors)
            {
                if (flavor.id == id) return flavor;
            }
            return null;
        }
    }
}
