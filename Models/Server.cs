﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Server
    {
        public string name { get; set; }
        public Image image { get; set;  }
        public Flavor flavor { get; set; }
        public List<Port> ports { get; set; }
        public string id { get; set; }


        public Server(string name, Image image, Flavor flavor, List<Port> ports, string id)
        {
            this.name = name;
            this.image = image;
            this.flavor = flavor;
            this.ports = ports;
            this.id = id;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
