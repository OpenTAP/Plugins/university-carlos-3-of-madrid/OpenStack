﻿using System.Collections.Generic;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Neutron
    {
        public List<Network> networks;
        public List<SecurityGroup> securityGroups;

        public Neutron()
        {
            networks = new List<Network>();
            securityGroups = new List<SecurityGroup>();
        }
    }
}
