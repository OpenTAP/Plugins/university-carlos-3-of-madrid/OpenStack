﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTap.Plugins.OpenStackPlugin.Models
{
    public class Network
    {
        public string name { get; set; }
        public string id { get; set; }

        public List<Subnet> subnets;

        public Network(string name, string id)
        {
            this.name = name;
            this.id = id;
            subnets = new List<Subnet>();
        }

        public void AddSubnet(Subnet subnet)
        {
            subnets.Add(subnet);
        }

        public void DeleteSubnet(Subnet subnet)
        {
            if (subnet == null) return;
            subnets.Remove(subnet);
        }

        public override string ToString()
        {
            return name;
        }
    }
}
